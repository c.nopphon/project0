@extends('template.template')

@section('title', ' ลงทะเบียน')
@section('content')
    @if(session('warning'))
        <div class="alert alert-{{session('warning')}}" role="alert">
            {{session('message')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="container">
        <div class="row">
            <h2></h2>
        </div>


    <form class="form-horizontal" action="{{action('AddListControllers@addName')}}" method="post">
        <fieldset>
            <!-- Form Name -->
            <legend> ลงทะเบียนใหม่</legend>
            <!-- Text input-->
            <div class="control-group col-md-6">
                <label class="control-label" for="Username">รหัสนักศึกษา :</label>
                <div class="controls">
                    <input name="student_id" type="number" class="form-control" required autocomplete="no">
                </div>
            </div>
            <div class="control-group col-md-6">
                <label class="control-label" for="Username">หลักศูตร : </label>
                <div class="controls">
                   <select class="form-control" name="level" required>
                       @foreach ($listLevel as $index => $val)
                           <option  value="{{$val->LEVELID}}" {{($val->LEVELID == 21) ? 'selected':''}}>{{$val->LEVELNAME}}</option>
                       @endforeach
                   </select>

                </div>
            </div>
            <div class="control-group col-md-6">
                <label class="control-label" for="Username">สาขา : </label>
                <div class="controls">
                    <select class="form-control" name="program" required>
                        <option value="" selected>เลือก</option>
                        @foreach ($listProgram as $index => $val)
                            <option  value="{{$val->PROGRAMID}}">{{$val->PROGRAMNAME}}</option>
                        @endforeach
                    </select>

                </div>
            </div>
            <div class="control-group col-md-6">
                <label class="control-label" for="Username">ปีการศึกษา : </label>
                <div class="controls">
                    <select class="form-control" name="admitacadyear" required>
                        @foreach ($admitacadyear as $index)
                            <option  value="{{$index}}" {{($index == date('Y')+543 ? 'selected':'')}}>{{$index}}</option>
                        @endforeach
                    </select>

                </div>
            </div>
            <div class="control-group col-md-4">
                <label class="control-label" for="Username">คำนำหน้า : </label>
                <div class="controls">
                    <select class="form-control" name="prefix">
                        @foreach ($listPrefix as $index => $val)
                            <option  value="{{$val->PREFIXID}}">{{$val->PREFIXNAME}}</option>
                        @endforeach
                    </select>

                </div>
            </div>
            <div class="control-group col-md-4">
                <label class="control-label" for="Username">ชื่อ : </label>
                <div class="controls">
                    <input  name="fname" type="text" placeholder="" class="form-control" required autocomplete="no">

                </div>
            </div>
            <!-- Password input-->
            <div class="control-group col-md-4">
                <label class="control-label" for="Password">สกุล : </label>
                <div class="controls">
                    <input  name="lname" type="text" placeholder="" class="form-control">
                    {{ csrf_field() }}
                </div>
            </div>
            <!-- Button -->
            <div class="control-group col-md-12 text-right">
                <label class="control-label" for="singlebutton"></label>
                <div class="controls">
                    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Save</button>
                    <button type="reset" id="reset" name="reset" class="btn btn-default">Reset</button>
                </div>
            </div>
        </fieldset>
    </form>
    </div>

@endsection
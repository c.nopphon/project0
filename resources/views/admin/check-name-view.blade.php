@extends('template.template')

@section('title', 'Dashboard')
@section('content')
    <script> $('#load-page').css('display','none');</script>
    @if(session('warning'))
        <div class="alert alert-{{session('warning')}}" role="alert">
            {{session('message')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if($warning == 'danger')
        <div class="alert alert-{{$warning}}" role="alert">
            {{$message}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @else
        <div class="col-md-12 text-right"><p><a href="{{url('admin/activity/'.$idActivity)}}" class="btn btn-info">ย้อนกลับ</a></p></div>
        <div class="col-sm-12" align="left">
            <h4> โครงการ/กิจกรรม : <span style="color:#ff0f00">{{$activity->name_activity}}</span></h4>
            <h4>จำนวน : {{$countPeople}} คน</h4>
            <p>เริ่ม : {{$activity->start}} | สิ้นสุด : {{$activity->end}}</p>

        </div>
    <link href="{{url('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
                <table id="addActivity" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                    <tr>
                        <th>ชื่อ - สกุล</th>
                        <th>ชั้นปี</th>
                        <th>เช็คอิน</th>
                        <th>#</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i=1?>
                    @foreach($name as $index => $val)
                        <tr>
                            <td>{{$val->PREFIXNAME}}{{$val->STUDENTNAME}} {{$val->STUDENTSURNAME}}</td>
                            <td></td>
                            <td>{{$val->DATE}}</td>
                            <td class="text-center">
                                <a onclick="document.getElementById('idDelete').value = '{{$val->ID}}'" data-title="Delete" data-toggle="modal" data-target="#delete"  href="" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>ชื่อ - สกุล</th>
                        <th>ชั้นปี</th>
                        <th>เช็คอิน</th>
                        <th>#</th>
                    </tr>
                    </tfoot>
                </table>
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{action('CheckNameSutdentControllers@deleteNameInActivity')}}" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                        <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
                        <input type="hidden" name="idDelete" id="idDelete">
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-danger">
                            <h4><span class="glyphicon glyphicon-warning-sign"></span> คำเตือน!!</h4>
                            <p>- รายชื่อที่เคยเข้าร่วมกิจกรรมก็จะถูกลบไปด้วย</p>
                        </div>


                    </div>
                    <div class="modal-footer ">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> ยืนยันการลบ</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
        <script>
            $(document).ready(function() {
                $('#addActivity').DataTable({
                    pageLength: 100,
                    "order" : [[ 2, "desc" ]]
                })
            } );
        </script>
    @endif
@endsection

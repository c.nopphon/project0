<html>
<head>
    <title>WebCamScanCode - โรงเรียนอวกาศ</title>
    @include ('template.meta')
    <style>
        #QR-Code{
        width: 100%;
        padding: 0px;
        }

    </style>
</head>
<body>
<link href="{{url('css/style.css')}}" rel="stylesheet">
<div class="container" id="QR-Code">
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="navbar-form navbar-left">
                <h4>WebCamScanCode ชมรมนักศึกษาวิชาทการ</h4>
            </div>
            <div class="navbar-form navbar-right">
             กล้องที่เชื่ือต่ออยู่
                <select class="form-control" id="camera-select"></select>
                <div class="form-group">
                    <button title="Play" class="btn btn-success btn-sm" id="play" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-play"></span></button>
                    <div style="display: none">
                        <input id="image-url" type="text" class="form-control" placeholder="Image url">
                        <button title="Decode Image" class="btn btn-default btn-sm" id="decode-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-upload"></span></button>
                        <button title="Pause" class="btn btn-warning btn-sm" id="pause" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-pause"></span></button>
                        <button title="Stop streams" class="btn btn-danger btn-sm" id="stop" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-stop"></span></button>
                        <button title="Image shoot" class="btn btn-info btn-sm disabled" id="grab-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-picture"></span></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body text-center">
            <div class="col-md-6">
                <div class="well" style="position: relative;display: inline-block;">
                    <canvas width="320" height="240" id="webcodecam-canvas"></canvas>
                    <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                    <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                    <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                    <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="thumbnail" id="result">
                    <div class="well" style="overflow: hidden;">
                        <img width="320" height="240" id="scanned-img" src="">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="thumbnail" id="result">
                    <div class="caption">
                        <h3>Scanned Code</h3>
                        <h4 id="scanned-QR"></h4>
                                <input class="form-control" id="scannedQrInput" type="text" style="width: 200px;    display: inline-block;" onkeyup="checkIdNst()" autofocus>
                                <button class="btn btn-success" id="bthSum" onClick="opener.document.all.id_student.value = document.all.scannedQrInput.value;opener.document.all.btn.click();">เช็คชื่ือ</button>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="{{url('js/webcodecam/filereader.js')}}"></script>
        <script type="text/javascript" src="{{url('js/webcodecam/qrcodelib.js')}}"></script>
        <script type="text/javascript" src="{{url('js/webcodecam/webcodecamjs.js')}}"></script>
        <script type="text/javascript" src="{{url('js/webcodecam/main.js')}}"></script>
    </div>
</div>
<script>
    function checkIdNst() {
        var scannedQrInput = document.getElementById("scannedQrInput");
        var bthSum = document.getElementById("bthSum");
        var x = scannedQrInput.value.length;
        if (x == 10) {
            bthSum.click();
        }
    }
    $('#play').click()
</script>

</body>
</html>


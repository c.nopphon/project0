<meta charset="utf-8">
<!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
<link rel="shortcut icon" href="http://โรงเรียนอวกาศ/logo.png" />
<title>@yield('title') -  โรงเรียนอวกาศ</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="{{url('css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{url('css/css.css')}}" rel="stylesheet">
<script type="text/javascript" src="{{url('js/jquery-1.11.1.min.js')}}"></script>
<script src="{{url('js/bootstrap.min.js')}}"></script>
<script src="{{url('js/jquery-3.3.1.js')}}"></script>
<script src="{{url('js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('js/dataTables.bootstrap4.min.js')}}"></script>

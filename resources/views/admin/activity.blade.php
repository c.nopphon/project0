@extends('template.template')

@section('title', 'รายการกิจกรรม')
@section('content')
    @if(session('warning'))
        <div class="alert alert-{{session('warning')}}" role="alert">
            {{session('message')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <link href="{{url('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <div class="row" style="display: none">
        <div class="col-md-4">
            <div class="dash-box dash-box-color-1">
                <div class="dash-box-icon">
                    <i class="glyphicon glyphicon-cloud"></i>
                </div>
                <div class="dash-box-body">
                    <span class="dash-box-title">ชั้นที่ปี 3</span>
                    <span class="dash-box-count">จำนวน : {{$sumScore3}} แต้ม</span>
                    <span class="dash-box-info">จำนวน : {{$activityNstClass3}} กิจกรรม</span>
                </div>
                <div class="dash-box-action">
                    {{--<button>More Info</button>--}}
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="dash-box dash-box-color-2">
                <div class="dash-box-icon">
                    <i class="glyphicon glyphicon-download"></i>
                </div>
                <div class="dash-box-body">
                    <span class="dash-box-title">ชั้นที่ปี 4</span>
                    <span class="dash-box-count">จำนวน : {{$sumScore4}} แต้ม</span>
                    <span class="dash-box-info">จำนวน : {{$activityNstClass4}} กิจกรรม</span>
                </div>

                <div class="dash-box-action">
                    {{--<button>More Info</button>--}}
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="dash-box dash-box-color-3">
                <div class="dash-box-icon">
                    <i class="glyphicon glyphicon-heart"></i>
                </div>
                <div class="dash-box-body">
                    <span class="dash-box-title">ชั้นที่ปี 5</span>
                    <span class="dash-box-count">จำนวน : {{$sumScore5}} แต้ม</span>
                    <span class="dash-box-info">จำนวน : {{$activityNstClass5}} กิจกรรม</span>
                </div>

                <div class="dash-box-action">
                    {{--<button>More Info</button>--}}
                </div>
            </div>
        </div>
    </div>
    <legend>ตารางกิจกรรม</legend>
    <div class="col-md-12 text-right"><p><a href="{{url('admin/add-activity')}}" class="btn btn-info">เพิ่มกิจกรรม</a>
        </p></div>
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th class="text-center"><span class="glyphicon glyphicon-bullhorn"></span></th>
            <th>กิจกรรม</th>
            <th>ชั้นปี</th>
            <th>แต้ม</th>
            <th>เริ่ม</th>
            <th>สิ้นสุด</th>
            <th>เข้าร่วม</th>
            <th>หมายเหตุ</th>
            <th>#</th>
        </tr>
        </thead>
        <tbody>
        @foreach($listAllActivity as $index => $val)
            <tr>
                <td class="text-center"><a onclick="alertBox({{$val->id}})" class="btn btn-warning btn-xs"
                                           data-title="Delete" data-toggle="modal" data-target="#news"><span
                                class="glyphicon glyphicon-bullhorn"></span></a></td>
                <td>{{$val->name_activity}}</td>
                <td>{{($val->nst_class == 0 ? 'ทุกปี' : $val->nst_class)}}</td>
                <td>{{$val->score}}</td>
                <td>{{$val->start}}</td>
                <td>{{$val->end}}</td>
                <td>{{$val->jonActivity}}</td>
                <td>{{$val->remark}}</td>
                <td class="text-center">
                    @if($val->status == 0)
                  <p>รออนุมัติ</p>
                        @elseif($val->status == 1)
                        <a href="{{url('admin/activity/'.$val->id)}}" class="btn btn-success btn-xs"><span
                                    class="glyphicon glyphicon-barcode"></span></a>
                        <a onmouseover="getMessage({{$val->id}})" class="btn btn-primary btn-xs" data-title="Edit"
                           data-toggle="modal" data-target="#edit"><span class="glyphicon glyphicon-pencil"></span></a>
                        <a onclick="document.getElementById('idDelete').value = '{{$val->id}}'" data-title="Delete"
                           data-toggle="modal" data-target="#delete" href="" class="btn btn-danger btn-xs"><span
                                    class="glyphicon glyphicon-trash"></span></a>
                    @endif
                </td>
            </tr>
        @endforeach

        </tbody>
        <tfoot>
        <tr>
            <th class="text-center"><span class="glyphicon glyphicon-bullhorn"></span></th>
            <th>กิจกรรม</th>
            <th>ชั้นปี</th>
            <th>แต้ม</th>
            <th>เริ่ม</th>
            <th>สิ้นสุด</th>
            <th>เข้าร่วม</th>
            <th>หมายเหตุ</th>
            <th>#</th>
        </tr>
        </tfoot>
    </table>
    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{action('AddActivityControllers@updateActivity')}}" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span
                                    class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                        <h4 class="modal-title custom_align" id="Heading">Edit Your Detail</h4>
                    </div>
                    <div class="modal-body">
                        <div class="control-group col-md-6">
                            <label class="control-label" for="Username">ชื่อกิจกรรม :</label>
                            <div class="controls">
                                <input name="name" type="text" class="form-control" required autocomplete="no">
                            </div>
                        </div>
                        <div class="control-group col-md-6">
                            <label class="control-label" for="Username">นศท. ชั้นปี : </label>
                            <div class="controls">
                                <select class="form-control" name="nst_class" required="">
                                    <option value="0">ทั้งหมด</option>
                                    @for ($i = 1; $i <= 8; $i++)
                                        <option value="{{ $i }}">ปี {{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <div class="control-group col-md-6">
                            <label class="control-label" for="Username">เริ่ม : </label>
                            <div class="controls">
                                <input name="start" type="datetime-local" class="form-control" required
                                       autocomplete="no" value="2019-07-17T22:22">
                            </div>
                        </div>
                        <div class="control-group col-md-6">
                            <label class="control-label" for="Username">สิ้นสุด : </label>
                            <div class="controls">
                                <input name="end" type="datetime-local" placeholder="" class="form-control" required
                                       autocomplete="no" value="">

                            </div>
                        </div>
                        <div class="control-group col-md-6">
                            <label class="control-label" for="Username">แต้ม : </label>
                            <div class="controls">
                                <input name="score" type="number" class="form-control" required autocomplete="no"
                                       value="1">
                            </div>
                        </div>
                        <div class="control-group col-md-6">
                            <label class="control-label" for="Password">หมายเหตุ : </label>
                            <div class="controls">
                                <input name="remark" type="text" placeholder="" class="form-control">
                                <input name="id" type="hidden" placeholder="" class="form-control">
                                {{ csrf_field() }}
                            </div>
                        </div>
                        <!-- Button -->
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer ">
                        <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span
                                    class="glyphicon glyphicon-ok-sign"></span> Update
                        </button>
                    </div>
            </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{action('ActivityControllers@deleteActivity')}}" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span
                                    class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                        <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
                        <input type="hidden" name="idDelete" id="idDelete">
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-danger">
                            <h4><span class="glyphicon glyphicon-warning-sign"></span> คำเตือน!!</h4>
                            <p>- รายชื่อที่เคยเข้าร่วมกิจกรรมก็จะถูกลบไปด้วย</p>
                        </div>


                    </div>
                    <div class="modal-footer ">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok-sign"></span> ยืนยันการลบ
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><span
                                    class="glyphicon glyphicon-remove"></span> ยกเลิก
                        </button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <div class="modal fade" id="news" tabindex="-1" role="dialog" aria-labelledby="news" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span
                                class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">Line notify</h4>
                    <input type="hidden" name="line_notify" id="line_notify">
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning" id="alertLine">
                        <h4><span class="glyphicon glyphicon-warning-sign"></span> <label id="message">แจ้งเตือน
                                !!</label></h4>
                        <p>ส่งข้อความแจ้งเตือนกิจกรรมทาง Line</p>
                    </div>
                </div>
                <div class="modal-footer ">
                    {{ csrf_field() }}
                    <button type="submit" onclick="lineNotify()" class="btn btn-success"><span
                                class="glyphicon glyphicon-ok-sign"></span> แจ้งเตือน
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span
                                class="glyphicon glyphicon-remove"></span> ยกเลิก
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <script>
        function alertBox(id) {
            document.getElementById('line_notify').value = id;
            $('#message').text('แจ้งเตือน !!');
            $("#alertLine").removeClass();
            $('#alertLine').addClass('alert alert-warning');

        }

        function getMessage(id) {
            $.ajax({
                type: 'POST',
                url: 'get-activity',
                data: {
                    _token: $("[name='_token']").val(),
                    id: id,
                },
                success: function (data) {
                    var dateSatrt = new Date(data.activity.start);
                    var dateEnd = new Date(data.activity.end);
                    $("[name='id']").val(data.activity.id);
                    $("[name='name']").val(data.activity.name_activity);
                    $("[name='nst_class']").val(data.activity.nst_class);
                    $("[name='start']").val(dateSatrt.getFullYear() + '-' + ("0" + (dateSatrt.getMonth() + 1)).slice(-2) + '-' + ("0" + dateSatrt.getDate()).slice(-2) + '\T' + dateSatrt.getHours() + ':' + (dateSatrt.getMinutes()<10?'0':'') + dateSatrt.getMinutes());
                    $("[name='end']").val(dateEnd.getFullYear() + '-' + ("0" + (dateEnd.getMonth() + 1)).slice(-2) + '-' + ("0" + dateEnd.getDate()).slice(-2) + '\T' + dateEnd.getHours() + ':' + (dateEnd.getMinutes()<10?'0':'') + dateEnd.getMinutes());
                    $("[name='score']").val(data.activity.score);
                    $("[name='remark']").val(data.activity.remark);
                }
            });
        }

        function lineNotify() {
            var id = $('#line_notify').val();
            $.ajax({
                type: 'POST',
                url: 'line-notify',
                data: {
                    _token: $("[name='_token']").val(),
                    id: id,
                },
                success: function (data) {
                    $("#alertLine").removeClass('alert alert-warning');
                    $("#alertLine").addClass('alert alert-' + data.warning);
                    $("#message").text(data.message);


                }
            });
        }
    </script>
@endsection

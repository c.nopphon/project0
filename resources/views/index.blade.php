@extends('template.template-user.template')

@section('title', 'ตารางกิจกรรม')
<div class="container text-center">
    <img src="{{url('img/logo.png')}}" style="width: 100px">
</div>
<div class="text-center"><h3>ตารางกิจกรรม</h3></div>

@section('content')
    <link href="{{url('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <div class="clearfix"></div><br>
    <table id="addActivity" class="table table-striped table-bordered" style="width:100%;font-size: 12px">
        <thead>
        <tr>
            <th>#</th>
            <th>กิจกรรม</th>
            <th>ชั้นปี</th>
            <th>แต้ม</th>
            <th>เริ่ม</th>
            <th>สิ้นสุด</th>
            <th>เข้าร่วม</th>
            <th>หมายเหตุ</th>
        </tr>
        </thead>
        <tbody>
        @foreach($tableActivity as $index => $val)
            <tr>
                <td class="text-center"><a href="{{url('name-in-activity/'.$val->id)}}" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-eye-open" style="padding: 1px"></span></a></td>
                <td>{{$val->name_activity}}</td>
                <td>{{($val->nst_class == 0 ? 'ทุกปี' : $val->nst_class)}}</td>
                <td>{{$val->score}}</td>
                <td>{{$val->start}}</td>
                <td>{{$val->end}}</td>
                <td>{{$val->jonActivity}}</td>
                <td>{{$val->remark}}</td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <th>#</th>
            <th>กิจกรรม</th>
            <th>ชั้นปี</th>
            <th>แต้ม</th>
            <th>เริ่ม</th>
            <th>สิ้นสุด</th>
            <th>เข้าร่วม</th>
            <th>หมายเหตุ</th>
        </tr>
        </tfoot>
    </table>
    <div class="modal fade" id="search" tabindex="-1" role="dialog" aria-labelledby="search" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{action('TableActivityControllers@searchActivityJoinByIdStudent')}}" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span
                                    class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                        <h4 class="modal-title custom_align" id="Heading">ค้นหา :</h4>
                    </div>
                    <div class="modal-body">
                        <div class="control-group col-md-12">
                            <label class="control-label" for="Username">รหัสประจำตัว :</label>
                            <div class="controls">
                                <input name="id_student" type="number" class="form-control" required autocomplete="no">
                                {{ csrf_field() }}
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- Button -->
                    <br>
                    <div class="clearfix"></div>
                    <div class="modal-footer ">
                        <button type="submit" class="btn btn-warning btn-lg"><span
                                    class="glyphicon glyphicon-ok-sign"></span> ค้นหา
                        </button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
<script>
    $(document).ready(function () {
        $('#addActivity').DataTable({
            pageLength: 25,
            "order": [[4, "desc"]]
        })
    });
</script>
<script type="text/javascript" src="{{url('/js/javascript.js')}}"></script>


@endsection


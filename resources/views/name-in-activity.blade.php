@extends('template.template-user.template')

@section('title', 'ตารางกิจกรรม')
<div class="container text-center">
    <img src="{{url('img/logo.png')}}" style="width: 100px">
</div>
@section('content')
    @if($warning == 'danger')
        <div class="clearfix"> </div>
        <br>
        <div class="alert alert-{{$warning}}" role="alert">
            {{$message}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

    @else
    <div class="form-group">
        <div class="col-sm-3"></div>
        <div class="col-sm-12">
            <h4> โครงการ/กิจกรรม : <span style="color:#ff0f00">{{$activity->name_activity}}</span></h4>
            <p>เริ่ม : {{$activity->start}} | สิ้นสุด : {{$activity->end}}</p>
            <p>ทั้งหมด : {{$count}} คน</p>
        </div>
        <input name="id_activity" type="hidden" value="{{$activity->id}}">
        {{ csrf_field() }}
    </div>
    <link href="{{url('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <div class="clearfix"></div><br>
    <table id="addActivity" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>ชื่อ - สกุล</th>
            <th>ชั้นปี</th>
            <th>เช็คอิน</th>
        </tr>
        </thead>
        <tbody>
        <?php $i=1?>
        @foreach($name as $index => $val)
            <tr>
                <td>{{$val->PREFIXNAME}}{{$val->STUDENTNAME}} {{$val->STUDENTSURNAME}}</td>
                <td></td>
                <td>{{$val->DATE}}</td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <th>ชื่อ - สกุล</th>
            <th>ชั้นปี</th>
            <th>เช็คอิน</th>

        </tr>
        </tfoot>
    </table>
    <div class="modal fade" id="search" tabindex="-1" role="dialog" aria-labelledby="search" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{action('TableActivityControllers@searchActivityJoinByIdStudent')}}" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span
                                    class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                        <h4 class="modal-title custom_align" id="Heading">ค้นหา :</h4>
                    </div>
                    <div class="modal-body">
                        <div class="control-group col-md-12">
                            <label class="control-label" for="Username">รหัสประจำคัว :</label>
                            <div class="controls">
                                <input name="id_student" type="number" class="form-control" required autocomplete="no">
                                {{ csrf_field() }}
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- Button -->
                    <br>
                    <div class="clearfix"></div>
                    <div class="modal-footer ">
                        <button type="submit" class="btn btn-warning btn-lg"><span
                                    class="glyphicon glyphicon-ok-sign"></span> ค้นหา
                        </button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
<script>
    $(document).ready(function () {
        $('#addActivity').DataTable({
            pageLength: 50,
            "order": [[2, "desc"]]
        })
    });
</script>
<script type="text/javascript" src="{{url('/js/javascript.js')}}"></script>
    @endif
@endsection


<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', 'TableActivityControllers@index');

Route::get('name-in-activity/{id}', 'TableActivityControllers@searchNameById');
Route::post('joint-activity', 'TableActivityControllers@searchActivityJoinByIdStudent');
Route::get('admin', 'LoginControllers@index');
Route::post('admin/login', 'LoginControllers@login');
Route::group(['middleware' => ['xx']], function () {
    Route::prefix('admin')->group(function () {
        Route::get('activity', 'ActivityControllers@index');
        Route::post('get-activity', 'ActivityControllers@getActivity');
        Route::post('activity/delete', 'ActivityControllers@deleteActivity');
        Route::post('line-notify', 'ActivityControllers@notifyMessage');
        Route::get('activity/{id}', 'CheckNameSutdentControllers@index');
        Route::get('activity/{id}/check-name-view', 'CheckNameSutdentControllers@checkNameViewAll');
        Route::post('activity/check-name', 'CheckNameSutdentControllers@checkNameByStudentId');
        Route::post('activity/check-name/delete', 'CheckNameSutdentControllers@deleteNameInActivity');
        Route::get('list-all-people', 'ListAllPeopleControllers@index');
        Route::get('list-all-people/{id}', 'ListAllPeopleControllers@createQrCode');
        Route::post('list-all-people/delete', 'ListAllPeopleControllers@deleteName');
        Route::post('list-all-people/update-student-code', 'AddListControllers@updateStudentCode');
        Route::post('list-all-people/getName', 'ListAllPeopleControllers@getName');
        Route::get('add-list', 'AddListControllers@index');
        Route::post('add-list/add-name', 'AddListControllers@addName');
        Route::post('add-list/update-name', 'AddListControllers@updateName');
        Route::get('add-activity', 'AddActivityControllers@index');
        Route::post('add-activity/add-activity', 'AddActivityControllers@AddActivity');
        Route::post('activity/update-activity', 'AddActivityControllers@updateActivity');
        Route::get('logout', 'LoginControllers@logOut');
        Route::get('scannedQrInput', function (){
            return view('admin.scannedQrInput');
        });
    });
});

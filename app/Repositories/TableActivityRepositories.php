<?php
/**
 * Created by PhpStorm.
 * User: support
 * Date: 2019-07-16
 * Time: 09:35
 */

namespace App\Repositories;
use DB;

class TableActivityRepositories
{
    public function getAllActivity()
    {
        return DB::table('tb_activity')
            ->leftJoin('td_activity_std', 'tb_activity.id', '=', 'td_activity_std.ID_ACTIVITY')
            ->selectRaw('tb_activity.*, count(td_activity_std.ID_ACTIVITY) as jonActivity')
            ->groupBy('tb_activity.id')
            ->where('status','=','1')
            ->limit(10)
            ->orderBy('start', 'desc')
            ->get();
    }
    public function getNameByActivityId($id){
        return DB::table('td_activity_std')
            ->leftJoin('tb_std', 'td_activity_std.ID_STUDENT', '=', 'tb_std.ID')
            ->leftJoin('tb_prefix', 'tb_std.PREFIXID', '=', 'tb_prefix.PREFIXID')
            ->select(
                "td_activity_std.ID",
                "td_activity_std.DATE",
                "tb_prefix.PREFIXNAME",
                "tb_std.STUDENTNAME",
                "tb_std.STUDENTSURNAME"

            )
            ->where('td_activity_std.ID_ACTIVITY','=',$id)
            ->orderBy('td_activity_std.DATE', 'DESC')
            ->get();
    }
    public function getActivityByIdStudent($id)
    {

        return DB::table('tb_std')
            ->leftJoin('td_activity_std', 'tb_std.ID', '=', 'td_activity_std.ID_STUDENT')
            ->leftJoin('tb_activity', 'td_activity_std.ID_ACTIVITY', '=', 'tb_activity.id')
            ->select(
                'tb_activity.name_activity',
                'tb_activity.score',
                'td_activity_std.DATE'
            )
            ->where('tb_std.STUDENTCODE', '=', $id)
            ->get();
    }
        public function gedStudent($id){
            return DB::table('tb_std')
                ->leftJoin('tb_prefix', 'tb_std.PREFIXID', '=', 'tb_prefix.PREFIXID')
                ->select(
                    'tb_prefix.PREFIXNAME',
                    'tb_std.STUDENTCODE',
                    'tb_std.STUDENTNAME',
                    'tb_std.STUDENTSURNAME'
                )
                ->where('tb_std.STUDENTCODE','=',$id)
                ->get()->first();
    }
}

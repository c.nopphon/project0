<?php
/**
 * Created by PhpStorm.
 * User: support
 * Date: 2019-07-16
 * Time: 09:35
 */

namespace App\Repositories;
use DB;

class ActivityRepositories
{
    public function getAllActivity()
    {
        return DB::table('tb_activity')
            ->leftJoin('td_activity_std', 'tb_activity.id', '=', 'td_activity_std.ID_ACTIVITY')
            ->selectRaw('tb_activity.*, count(td_activity_std.ID_ACTIVITY) as jonActivity')
            ->groupBy('tb_activity.id')
            ->where('status','<=','1')
            ->orderBy('start', 'desc')
            ->get();
    }
    public function getActivityByNstClass($nstClass)
    {
        return DB::table('tb_activity')
            ->leftJoin('td_activity_std', 'tb_activity.id', '=', 'td_activity_std.ID_ACTIVITY')
            ->selectRaw('tb_activity.*, sum(tb_activity.score) as jonActivity')
            ->groupBy('tb_activity.id')
            ->where('status','=','1')
            ->where('nst_class','=','1')
            ->orWhere('nst_class', '=', $nstClass)
            ->orderBy('start', 'desc')
            ->get();
    }
    public function deleteActivityAndNameInActivity($id){
        $deleteNameInActivity = DB::table('td_activity_std')->where('ID_ACTIVITY', $id)->delete();
        DB::table('tb_activity')->where('id', $id)->delete();
        if($deleteNameInActivity == 0){
            return true;
        } else {
            return false;
        }

    }
    public function getActivityById($id)
    {
        return DB::table('tb_activity')
            ->where('id','=',$id)
            ->orderBy('start', 'desc')
            ->get()->first();
    }
}

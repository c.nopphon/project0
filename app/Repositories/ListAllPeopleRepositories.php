<?php
/**
 * Created by PhpStorm.
 * User: OHnoBel
 * Date: 2019-07-14
 * Time: 15:28
 */

namespace App\Repositories;
use DB;

class ListAllPeopleRepositories
{
    public function getAllStudent()
    {
        return DB::table('tb_std')
            ->leftJoin('tb_program', 'tb_std.PROGRAMID', '=', 'tb_program.PROGRAMID')
            ->leftJoin('tb_level', 'tb_std.LEVELID', '=', 'tb_level.LEVELID')
            ->leftJoin('tb_prefix', 'tb_std.PREFIXID', '=', 'tb_prefix.PREFIXID')
            ->select(
                "tb_std.ID",
                "tb_std.STUDENTCODE",
                "tb_level.LEVELNAME",
                "tb_program.PROGRAMNAME",
                "tb_prefix.PREFIXNAME",
                "tb_std.STUDENTNAME",
                "tb_std.STUDENTSURNAME",
                "tb_std.ADMITACADYEAR"
            )
            ->where('tb_std.ADMITACADYEAR','>=','2559')
            ->get();
    }
    public function deleteNameAndNameInActivity($id){
        $deleteNameInActivity = DB::table('td_activity_std')->where('ID_STUDENT', $id)->delete();
        DB::table('tb_std')->where('ID', $id)->delete();
        if($deleteNameInActivity == true){
            return true;
        } else {
            return false;
        }
    }
    public function getNameById($id)
    {
        return DB::table('tb_std')
            ->where('ID','=',$id)
            ->get()->first();
    }
    public function getNameByIdStd($id)
    {
        return DB::table('tb_std')
            ->leftJoin('tb_prefix', 'tb_std.PREFIXID', '=', 'tb_prefix.PREFIXID')
            ->select(
                "tb_std.STUDENTCODE",
                "tb_prefix.PREFIXNAME",
                "tb_std.STUDENTNAME",
                "tb_std.STUDENTSURNAME",
                "tb_std.ADMITACADYEAR"
            )
            ->where('ID','=',$id)
            ->get()->first();
    }
}

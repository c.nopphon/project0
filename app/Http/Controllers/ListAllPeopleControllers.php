<?php
/**
 * Created by PhpStorm.
 * User: OHnoBel
 * Date: 2019-07-14
 * Time: 15:25
 */

namespace App\Http\Controllers;
use App\Libraries\ListAllPeopleLibraries;
use App\Repositories\ListAllPeopleRepositories;
use App\Repositories\AddListRepositories;
use Illuminate\Http\Request;

class ListAllPeopleControllers
{
    public $ListAllPeopleLibrary;
    public $ListAllPeopleRepositories;
    public $AddListRepositories;


    public function __construct(ListAllPeopleLibraries $ListAllPeopleLibrary, ListAllPeopleRepositories $ListAllPeopleRepositories, AddListRepositories $AddListRepositories)
    {
        $this->ListAllPeopleLibrary = $ListAllPeopleLibrary;
        $this->ListAllPeopleRepositories = $ListAllPeopleRepositories;
        $this->AddListRepositories = $AddListRepositories;


    }

    public function index(){

        $result = $this->ListAllPeopleRepositories->getAllStudent();
        $listLevel = $this->AddListRepositories->getAllLevel();
        $listProgram = $this->AddListRepositories->getAllProgram();
        $listPrefix = $this->AddListRepositories->getAllPrefix();
        $admitacadyear = array(
            2564,2543,2562,2561,2560,2559
        );
      return view('admin.list-all-people',[
          'listAllPeople' => $result,
          'listLevel' => $listLevel,
          'listProgram' => $listProgram,
          'listPrefix' => $listPrefix,
          'admitacadyear' => $admitacadyear
      ]);
}
    public function deleteName(Request $request)
    {
        $result = $this->ListAllPeopleRepositories->deleteNameAndNameInActivity($request->get('idDelete'));
        if ($result) {
            return back()
                ->with('warning', 'warning')
                ->with('message', 'ลบรายชื่อสำเร็จ!!');
        } else {
            return back()
                ->with('warning', 'danger')
                ->with('message', 'ลบรายชื่อไม่สำเร็จ!!');
        }
    }

    public function getName(Request $request){
        $result = $this->ListAllPeopleRepositories->getNameById($request->get('id'));
        return response()->json(['names' => $result]);
    }

    public function createQrCode($id){
            $nameStd = $this->ListAllPeopleRepositories->getNameByIdStd($id);
            return view('admin.create-qr-code',[
                'student' => $nameStd
                ]);
    }

}
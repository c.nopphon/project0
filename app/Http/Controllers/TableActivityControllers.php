<?php
/**
 * Created by PhpStorm.
 * User: support
 * Date: 2019-07-18
 * Time: 16:51
 */

namespace App\Http\Controllers;
use App\Repositories\TableActivityRepositories;
use App\Repositories\CheckNameSutdentRepositories;
use Illuminate\Http\Request;

class TableActivityControllers
{
    public $TableActivityRepositories;
    public function __construct(TableActivityRepositories $TableActivityRepositories)
    {
        $this->TableActivityRepositories = $TableActivityRepositories;
    }

    public function index()
    {
        $tableActivity = new TableActivityRepositories();
        $result = $tableActivity->getAllActivity();
        return view('index', [
            'tableActivity' => $result
        ]);
    }

    public function searchNameById($id){
        $CheckNameSutdentRepositories = new CheckNameSutdentRepositories();
        $getActivity = $CheckNameSutdentRepositories->getActivityById($id);
        $getName = $this->TableActivityRepositories->getNameByActivityId($id);
        $countRow = count($getName);
        if ($getActivity != null) {
            return view('name-in-activity', [
                'name' => $getName,
                'count' => $countRow,
                'activity' => $getActivity,
                'warning' => '',
            ]);
        } else {
            return view('name-in-activity')
                ->with('warning', 'danger')
                ->with('message', 'ไม่พบกิจกรรม');
        }
    }

    public function searchActivityJoinByIdStudent(Request $request){
        $id = $request->get('id_student');
        $gedStudent = $this->TableActivityRepositories->gedStudent($id);

        if($gedStudent == null){
            return view('joint-activity')
                ->with('warning', 'danger')
                ->with('message', 'ไม่พบ รหัสประจำตัว');
        } else {
            $nameActivity = $this->TableActivityRepositories->getActivityByIdStudent($id);
            $sumScore = 0;
            foreach ($nameActivity as $index => $val){
                $sumScore += $nameActivity[$index]->score;
            }

            return view('joint-activity', [
                'activity' => $nameActivity,
                'sumScore' => $sumScore,
                'name' => $gedStudent,
                'warning' => '',
            ]);
        }

    }

}

<?php
/**
 * Created by PhpStorm.
 * User: OHnoBel
 * Date: 2019-07-14
 * Time: 18:06
 */

namespace App\Http\Controllers;
use App\Libraries\ListAllPeopleLibraries;
use App\Libraries\CenterLibraries;
use App\Libraries\AddListLibraries;
use App\Repositories\AddListRepositories;
use Illuminate\Http\Request;


class AddListControllers
{
    public $ListAllPeopleLibrary;
    public $AddListRepositories;
    public $CenterLibraries;
    public $AddListLibraries;

    public function __construct(ListAllPeopleLibraries $ListAllPeopleLibrary, AddListRepositories $AddListRepositories, CenterLibraries $CenterLibraries, AddListLibraries $AddListLibraries)
    {
        $this->ListAllPeopleLibrary = $ListAllPeopleLibrary;
        $this->AddListRepositories = $AddListRepositories;
        $this->CenterLibraries = $CenterLibraries;
        $this->AddListLibraries = $AddListLibraries;


    }

    public function index(){
        $listLevel = $this->AddListRepositories->getAllLevel();
        $listProgram = $this->AddListRepositories->getAllProgram();
        $listPrefix = $this->AddListRepositories->getAllPrefix();
        $admitacadyear = array(
           2562,2561,2560,2559,2558,2557,2556,2555
        );
        return view('admin.add-list', [
            'listLevel' => $listLevel,
            'listProgram' => $listProgram,
            'listPrefix' => $listPrefix,
            'admitacadyear' => $admitacadyear
        ]);
    }

    public function addName(Request $request){
        $result = $this->AddListRepositories->getIdStudent(trim($request->get('student_id')));
        if ($result == null) {
            $data =
                [
                    'STUDENTCODE' => $request->get('student_id'),
                    'LEVELID' => $request->get('level'),
                    'PROGRAMID' => $request->get('program'),
                    'PREFIXID' => $this->CenterLibraries->slug($request->get('prefix')),
                    'STUDENTNAME' => trim($request->get('fname')),
                    'STUDENTSURNAME' => trim($request->get('lname')),
                    'ADMITACADYEAR' => $request->get('admitacadyear'),
                    'IP' => $request->ip(),

                ];
            $this->AddListRepositories->save($data);
            return back()
                ->with('warning', 'success')
                ->with('message', 'ลงทะเบียนสำเร็จ');

        } else {
            return back()
                ->with('warning', 'danger')
                ->with('message', 'ลงทะเบียนไม่สำเร็จ !');

        }
    }
    public function updateName(Request $request){
        $result = $this->AddListLibraries->checkDuplicateStudentId($request->get('id'), $request->get('student_id'));

        if ($result == true) {
            $data =
                [
                    'STUDENTCODE' => $request->get('student_id'),
                    'LEVELID' => $request->get('level'),
                    'PROGRAMID' => $request->get('program'),
                    'PREFIXID' => $this->CenterLibraries->slug($request->get('prefix')),
                    'STUDENTNAME' => trim($request->get('fname')),
                    'STUDENTSURNAME' => trim($request->get('lname')),
                    'ADMITACADYEAR' => $request->get('admitacadyear'),
                    'IP' => $request->ip(),

                ];
            $this->AddListRepositories->update($request->get('id'), $data);
            return back()
                ->with('warning', 'success')
                ->with('message', 'อัพเดทรายชื่อสำเร็จ!');

        } else {
            return back()
                ->with('warning', 'danger')
                ->with('message', 'อัพเดทรายชื่อไม่สำเร็จ');

        }
    }
    public function updateStudentCode(Request $request)
    {
        $result = $this->AddListLibraries->checkDuplicateStudentId($request->get('id'), $request->get('student_id'));

        if ($result == true) {
            $data =
                [
                    'STUDENTCODE' => $request->get('student_id'),

                ];
            $this->AddListRepositories->update($request->get('id'), $data);
            return response()->json([
                'warning'  => 'success',
                'message'  => 'อัพเดทรหัสนักศึกษาสำเร็จ',
                'codeStu' => $request->get('student_id')

            ]);
        } else {
            return response()->json([
                'warning'  => 'danger',
                'message'  => 'อัพเดทรหัสนักศึกษาไม่สำเร็จ',
                'codeStu' => $request->get('student_id')
            ]);

        }
    }

}
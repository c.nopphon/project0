<?php
/**
 * Created by PhpStorm.
 * User: support
 * Date: 2019-07-17
 * Time: 11:39
 */

namespace App\Libraries;
use App\Repositories\CheckNameSutdentRepositories;

class CheckNameSutdentLibraries
{
    public $NameInActivityRepositories;
    public function __construct(CheckNameSutdentRepositories $NameInActivityRepositories)
    {
        $this->NameInActivityRepositories = $NameInActivityRepositories;
    }

    public function checkStudentIdAndActivityId($id_activity, $id_student)
    {
        $id['Activity'] = $this->NameInActivityRepositories->getIdActivityById($id_activity);
        $id['StudentId'] = $this->NameInActivityRepositories->getIdStudentById($id_student);
        return $id;

    }

    public function checkStudentDuplicate($id_activity, $id_student){

    }

}
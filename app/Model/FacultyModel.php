<?php
/**
 * Created by PhpStorm.
 * User: OHnoBel
 * Date: 2019-07-14
 * Time: 18:17
 */

namespace App\Model;


class FacultyModel
{
    protected $table = 'tb_faculty';
    protected $fillable = [
        'FACULTYID',
        'FACULTYNAME',
    ];
}

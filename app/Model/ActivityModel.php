<?php
/**
 * Created by PhpStorm.
 * User: support
 * Date: 2019-07-15
 * Time: 16:44
 */

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class ActivityModel extends Model
{
    protected $table = 'tb_activity';
    protected $fillable = [
        'id',
        'ip',
        'start',
        'end',
        'name_activity',
        'slug',
        'nst_class',
        'score',
        'remark',
        'status',
    ];
}
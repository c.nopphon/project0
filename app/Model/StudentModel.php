<?php
/**
 * Created by PhpStorm.
 * User: OHnoBel
 * Date: 2019-07-14
 * Time: 15:28
 */

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class StudentModel extends Model
{
    protected $table = 'tb_std';
    protected $fillable = [
        'ID',
        'STUDENTCODE',
        'LEVELID',
        'PROGRAMID',
        'PREFIXID',
        'STUDENTNAME',
        'STUDENTSURNAME',
        'ADMITACADYEAR',
    ];
}